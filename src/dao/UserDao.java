package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("loginid");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch");
			sql.append(", section");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // id
			sql.append(", ?"); // loginid
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branch
			sql.append(", ?"); // section
			sql.append(", CURRENT_TIMESTAMP");//created_date
			sql.append(", CURRENT_TIMESTAMP");//updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginid());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setString(4, user.getBranch());
			ps.setString(5, user.getSection());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String loginid,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE loginid = ?  AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginid);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginid = rs.getString("loginid");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String branch = rs.getString("branch");
				String section = rs.getString("section");


				User user = new User();
				user.setId(id);
				user.setLoginid(loginid);
				user.setPassword(password);
				user.setName(name);
				user.setBranch(branch);
				user.setSection(section);


				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}