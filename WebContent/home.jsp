<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム</title>
	</head>
	<body>
		<div class="profile">
			<div class="name"><h2><c:out value="${loginUser.name}" />さん、</h2></div>
		</div>
		<h3>よくここまでこれたね。</h3>
		<div class="loginid">
			@<c:out value="${loginUser.loginid}" />
		</div>
		<div class="post"><a href="post">新規投稿</a></div>
		<a href="logout">ログアウト</a>
		<div class="humanresauce">
			<div class="signup"><a href="signup">ユーザー新規登録（総務部用）</a></div>
			<div class="useredit"><a href="useredit">ユーザー編集（総務部用）</a></div>
			<div class="usermanage"><a href="usermanage">ユーザー管理（総務部用）</a></div>
		</div>
		<div class="messages">
		<c:forEach items="${messages}" var="message">
			<div class="message">
				<div class="name">
					<span class="name"><c:out value="${message.name}" /></span>
				</div>
				<div class="category">カテゴリ：<c:out value="${message.category}" /></div>
				<div class="subject">題：<c:out value="${message.subject}" /></div>
				<div class="text"><c:out value="${message.text}" /></div>
				<div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
				<c:forEach items="${comments}" var="comment">
					<c:if test="${comment.message_id == message.id }">
					<div class="commentedUser"><c:out value="${comment.userName}"></c:out></div>
					<div class="commentText"><c:out value="${comment.text}"></c:out></div>
					<div class="commentedDate"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
					</c:if>
				</c:forEach>
				<form action="newComment" method="post">
				<div class="messageId"><input type="hidden" name="messageId" value="${message.id}"></div>
				<div class="newcomment"><input name="newcomment" id="newcomment" /><input type="submit" value="コメントする" /></div>
				</form>
				</div>
		</c:forEach>
		</div>
		<div class="copyright"> Copyright(c)株式会社○○</div>
	</body>
</html>