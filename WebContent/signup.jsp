<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>社員登録</title>
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
			<c:remove var="errorMessages" scope="session" />
			</c:if>
			<form action="signup" method="post">
				<br /> <label for="name">名前</label> <input name="name" id="name" />（フルネームでお願いします）<br />
				<label for="loginid">ログインID</label> <input name="loginid" id="loginid" /> <br />
				<label for="password">パスワード</label> <input name="password" type="password" id="password" /> <br />
				<label for="branch">支店番号</label> <select name="branch" id="branch">
					<option value="1">本社</option>
					<option value="2">支店A</option>
					<option value="3">支店B</option>
					<option value="4">支店C</option>  </select> <br />
				<label for="section">部署番号</label> <select name="section" id="section" >
					<option value="1">経営管理職</option>
					<option value="2">人事総務部</option>
					<option value="3">商品開発部</option>
					<option value="4">財務部</option>
					<option value="5">営業部・支店長</option>
					<option value="6">営業部・一般社員</option> </select> <br />
				<input type="submit" value="登録" /> <br /> <a href="home">戻る</a>
			</form>
			<div class="copyright">Copyright(c)株式会社○○</div>
		</div>
	</body>
</html>